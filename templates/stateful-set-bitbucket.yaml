{{ if .Values.bitbucket.deploy }}
kind: StatefulSet
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: bitbucket
  name: bitbucket
spec:
  selector:
    matchLabels:
      k8s-app: bitbucket
  serviceName: bitbucket
  template:
    metadata:
      labels:
        k8s-app: bitbucket
    spec:
      initContainers:
      - name: prepare-config
        imagePullPolicy: Always
        image: {{ .Values.bitbucket.image.repository }}:{{ .Values.bitbucket.image.tag }}
        command: ["/bin/bash"]
        args: ["-c", "if [ ! -f /var/atlassian/application-data/bitbucket/shared/bitbucket.properties ]; then mkdir -p /var/atlassian/application-data/bitbucket/shared && cp /tmp/bitbucket.properties /var/atlassian/application-data/bitbucket/shared/bitbucket.properties && chown -R 2003:2003 /var/atlassian/application-data/bitbucket && chmod -R 777 /var/atlassian/application-data/bitbucket; else exit 0; fi"]
        volumeMounts:
        - name: bitbucket-data
          mountPath: /var/atlassian/application-data/bitbucket/shared
        - name: bitbucket-properties
          mountPath: /tmp/bitbucket.properties
          subPath: bitbucket.properties
{{ if .Values.bitbucket.datacenter }}
      - name: hazelcast-config
        image: yivantsov/py-init-atl
        command: ["python"]
        args: ["/tmp/get-pods-ips.py"]
        volumeMounts:
        - name: bitbucket-data
          mountPath: /var/atlassian/application-data/bitbucket/shared
        - name: hazelcast-config
          mountPath: /tmp/get-pods-ips.py
          subPath: get-pods-ips.py
{{ end }}
      - name: check-pg-ready
        image: postgres:9.6-alpine
        command: ['sh', '-c',
          'until pg_isready -h {{ .Values.bitbucket.db.host }} -p 5432;
          do echo waiting for database; sleep 2; done;
          {{ if .Values.bitbucket.db.createDb }}PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }} -c "create user {{ .Values.bitbucket.db.username }} with password ''{{ .Values.bitbucket.db.password }}''" || true;
          PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }}  -c "create database {{ .Values.bitbucket.db.database }}" || true;
          PGPASSWORD="{{ .Values.bamboo.db.password }}" psql -h postgres -U {{ .Values.bamboo.db.username }}  -c "grant all privileges on database {{ .Values.bitbucket.db.database }} to {{ .Values.bitbucket.db.username }}" || true {{ end }}']
      containers:
      - name: bitbucket
        image: {{ .Values.bitbucket.image.repository }}:{{ .Values.bitbucket.image.tag }}
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 7990
          protocol: TCP
        - containerPort: 7991
          protocol: TCP
        - containerPort: 5701
          protocol: TCP
        - containerPort: 7999
          protocol: TCP
        - containerPort: 7896
          protocol: TCP
        env:
{{ if .Values.bitbucket.mode.install }}
          - name: SETUP_DISPLAYNAME
            value: "{{ .Values.bitbucket.displayName }}"
          - name: SETUP_BASEURL
            value: "{{ if .Values.tls.use }}https{{ else }}http{{ end }}://{{ .Values.bitbucket.ingress.host }}"
          - name: SETUP_LICENSE
            value: "{{ .Values.bitbucket.license }}"
          - name: SETUP_SYSADMIN_USERNAME
            value: "{{ .Values.bitbucket.adminUser }}"
          - name: SETUP_SYSADMIN_PASSWORD
            value: "{{ .Values.bitbucket.adminPassword }}"
          - name: SETUP_SYSADMIN_DISPLAYNAME
            value: "{{ .Values.bitbucket.adminFullName }}"
          - name: SETUP_SYSADMIN_EMAILADDRESS
            value: "{{ .Values.bitbucket.adminEmail }}"
  {{ end }}
          - name: JDBC_DRIVER
            value: org.postgresql.Driver
          - name: JDBC_USER
            value: {{ .Values.bitbucket.db.username }}
          - name: JDBC_PASSWORD
            value: {{ .Values.bitbucket.db.password }}
          - name: JDBC_URL
            value: jdbc:postgresql://{{ .Values.bitbucket.db.host }}:{{ .Values.bitbucket.db.port }}/{{ .Values.bitbucket.db.database }}
          - name: JVM_MINIMUM_MEMORY
            value: {{ .Values.bitbucket.javaOpts.jvmMinimumMemory }}
          - name: JVM_MAXIMUM_MEMORY
            value: {{ .Values.bamboo.javaOpts.jvmMaximumMemory }}
          {{ if .Values.bitbucket.jvmSupportRecommendedArgs }}
          - name: JVM_SUPPORT_RECOMMENDED_ARGS
            value: "{{ .Values.bitbucket.jvmSupportRecommendedArgs }}"
          {{ end }}
          - name: SERVER_PORT
            value: "7990"
          - name: SERVER_PROXY_NAME
            value: "{{ .Values.bitbucket.ingress.host }}"
          - name: SERVER_PROXY_PORT
            value: "{{ if .Values.tls.use }}443{{ else }}80{{ end }}"
          - name: SERVER_SCHEME
            value: "{{ if .Values.tls.use }}https{{ else }}http{{ end }}"
          - name: SERVER_SECURE
            value: "{{ if .Values.tls.use }}true{{ else }}false{{ end }}"
{{ if .Values.bitbucket.datacenter }}
          - name: ELASTICSEARCH_ENABLED
            value: "false"
          - name: PLUGIN_SEARCH_ELASTICSEARCH_BASEURL
            value: {{ if .Values.bitbucket.deployElasticSearch }}http://elasticsearch:9200{{ else }}{{ .Values.bitbucket.elasticSearchUrl }}{{ end }}
          - name: PLUGIN_SEARCH_ELASTICSEARCH_USERNAME
            value: {{ .Values.bitbucket.elasticsearchUsername }}
          - name: PLUGIN_SEARCH_ELASTICSEARCH_PASSWORD
            value: {{ .Values.bitbucket.elasticsearchPassword }}
{{ end }}

        volumeMounts:
        - name: bitbucket-data
          mountPath: /var/atlassian/application-data/bitbucket/shared
        readinessProbe:
          tcpSocket:
            port: 7990
          initialDelaySeconds: 15
          periodSeconds: 5
          failureThreshold: 25
        resources:
          requests:
            memory: "{{ .Values.bitbucket.resources.requests.memory }}"
            cpu: "{{ .Values.bitbucket.resources.requests.cpu }}"
          limits:
            memory: "{{ .Values.bitbucket.resources.limits.memory }}"
            cpu: "{{ .Values.bitbucket.resources.limits.cpu }}"
      volumes:
      - name: bitbucket-data
        persistentVolumeClaim:
          claimName: bitbucket-data
      - name: bitbucket-properties
        configMap:
          name: bitbucket-properties
      - name: hazelcast-config
        configMap:
          name: hazelcast-config
{{ end }}
