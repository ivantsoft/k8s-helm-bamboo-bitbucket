# Atlassian Bamboo and Bitbucket Helm Chart
![K8s-Atlassian](k8s.png)
## Project State

**This IS NOT an official Helm chart!**

The project is in its **pre-alpha** state. **DO NOT deploy this to production** or any critical environment.

All deployments have been tested and validated on Digital Ocean Kubernetes and Minikube only. Next deployment targets are Amazon EKS, Google GKE, Azure AKS. Even though none of K8s objects in this chart are K8s vendor specific, it is always a good idea to test on as many environments as possible, especially when it comes to storage and external networking (LB, ingresses).

This Helm chart can definitely use multiple improvements. Upgrades were tested, but that's not enough.

It probably makes sense to decouple it into 2 individual charts later on.

## What It Is

This Helm chart deploys:

* [Bamboo server](https://www.atlassian.com/software/bamboo)
* [Bamboo remote agents](https://confluence.atlassian.com/bamboo/remote-agents-289277115.html) (optional)
* [Bitbucket Server](https://bitbucket.org/product/) or [Datacenter](https://www.atlassian.com/enterprise/data-center/bitbucket) (depending on the settings and license used)
* Postgres database
* Single node Elasticsearch instance

Deployments use a mix of images: [official ones](https://hub.docker.com/r/atlassian/bitbucket-server), images from [Mr. Dave Chevell](https://bitbucket.org/dchevell/docker-atlassian-bamboo/src/master/), and custom images (for Bamboo remote agents).

## Pre-Reqs

* Kubernetes cluster
* kubectl with configured context (cluster-admin required to create a ClusterRoleBinding)
* helm (tested with 3.x only)

### Minimal System Requirements

#### Bamboo server alone

* 1 cpu
* 2GB RAM
* 1 PV (RWO)

#### Bamboo server + 1 remote agent

* 1.5 cpu
* 3GB RAM
* 2 PV (RWO)

#### Bitbucket (with bundled Elasticsearch or a standalone one)

* 1.5 cpu
* 4GB RAM
* 1 PV (RWX)
* 1 PV (RWO)

You can do the maths for any combination of components. It is recommended to keep CPU and RAM requests for Bitbucket pretty high to make sure things aren't too slow.

You can adjust requests and limits either in values.yaml or as an argument to Helm command, for example:

```
--set bitbucket.resources.requests.cpu=300m --set bitbucket.resources.requests.memory=4Gi --set bitbucket.resources.limits.cpu=1500m --set bitbucket.resources.limits.memory=6Gi
```

Bitbucket uses a lot of CPU when starting, so the higher the limit the better. When on Minikube, bear in mind that your K8s components run in the same VM, so setting too high requests and limits can make K8s components starve.

## How to Deploy

### Create namespace

```
kubectl create namespace atlassian
```

### values.yaml

Review values.yaml to provide license (the default ones are dummy), hostnames, desired admin user name, password and other details. Make sure you go though the entire file and read comments there.

Install Bamboo and Bitbucket Datacenter with a standalone Elasticsearch with default settings, and link them:

```
helm install atlassian ./ -n atlassian --set bamboo.mode.install=true --set bitbucket.mode.install=true --set bitbucket.datacenter=true --set bitbucket.deployElasticSearch=true --set link=true
```

Watch install job logs. You may need to wait until the job pod is in the running state

```
kubectl logs -f job/install-bamboo -n atlassian
kubectl logs f job/link-bamboo-bitbucket -n atlassian
```

## Minikube

It is possible to deploy everything on Minikube for **chart evaluation purposes only**, however, the VM needs to be given at least 3 CPU and 9GB RAM (bundled Elasticsearch, do not try to deploy multiple replicas of Bitbucket unless you can share a lot of CPU and RAM with your Minikube VM):

```
minikube start --cpus=3 --memory=9G
```

Even with seemingly enough resources, it is highly recommended to deploy Bamboo first, increase CPU limit for Bitbucket and deploy bitbucket.

Deploy and install Bamboo:

```
helm install atlassian ./ -n atlassian --set bamboo.mode.install=true --set bitbucket.deploy=false --set link=false --set tls.use=false
```

Deploy Bitbucket, link Bamboo and Bitbucket:

```
helm upgrade atlassian ./ -n atlassian --set bamboo.deploy=false  --set bitbucket.deploy=true --set bitbucket.mode.install=true --set bitbucket.datacenter=false --set link=true --set bitbucket.resources.requests.cpu=300m --set bitbucket.resources.requests.memory=3Gi --set bitbucket.resources.limits.cpu=1500m --set bitbucket.resources.limits.memory=5Gi
```
Beware that increasing CPU limits may result in Minikube cluster degraded performance or even crashes. Perhaps, one out of 10 attempts to deploy everything on a 9GB/3CPU Minikube cluster was a success. Please, **deploy separately**, and be ready for things working way too slow and eventually crash if your Minikube VM does not have enough resources :)

## What Can Be Configured

There may be some mistakes in the tables. Look at values.yaml for the most complete list.

### General

| Parameter        | Description      | Default Value |
| -----------------|:-------------:| |
| tls.use      | use TLS in ingress objects. Make sure your ingress controller is configured with either a wildcard crt or you have tls secrets for your DNS names | true         |
| tls.certmanager  | if you have certmanager deployed it can be used to request crts from Letsencrypt      |  true |
| tls.certmanagerClusterissuer  | name of cert manager cluster issuer      |  letsencrypt-prod |


### Bamboo

| Parameter        | Description      | Default Value |
| -----------------|:-------------:| |
| bamboo.deploy            | deploy Atlassian Bamboo | true         |
| bamboo.mode.install      | run initial installation, connect to DB, create tables. Set to true only during the 1st run | false         |
| bamboo.image.repository  | image repo      |  dchevell/bamboo |
| bamboo.image.tag         | image tag       | see values.yaml  |
| bamboo.image.pullPolicy  | image pull policy      | IfNotPresent  |
| bamboo.license           | Bamboo license. Please mind indentation, see example in values.yaml      | none  |
| bamboo.serverId          | your Bamboo server ID which comes with Bamboo license     | none  |
| bamboo.serverKey         | your Bamboo server key      | 278  |
| bamboo.adminUser         | your Bamboo admin user      | admin  |
| bamboo.adminPassword     | your Bamboo admin password      | password  |
| bamboo.adminFullName     | your Bamboo admin full name      | Bamboo Admin  |
| bamboo.adminEmail        | your Bamboo admin email      | admin@atlassian.com  |
| bamboo.db.type           | database type ha/postgres     | postgres  |
| bamboo.db.deployPostgres | deploy Postgres or connect to existing database   | true  |
| bamboo.db.host           | database host     | postgres  |
| bamboo.db.port           | database port    | 5432  |
| bamboo.db.username       | database username    | bamboo  |
| bamboo.db.password       | database password     | password  |
| bamboo.db.database       | database name     | bamboo  |
| bamboo.ingress.enabled   | create ingress     | true  |
| bamboo.ingress.annotations | ingress annotations     | ingressClass: nginx,  nginx.ingress.kubernetes.io/ssl-redirect: "true"  |
| bamboo.ingress.host      | hostname for ingress spec     | ci.kubedemo.ml  |
| bamboo.storage.storageClassName      | StorageClass for Bamboo PVC     | none. default storage class will be used |
| bamboo.storage.request      | request for storage     | 5Gi  |
| bamboo.resources.limits.cpu | CPU resource limits for Bamboo container     | 3000m  |
| bamboo.resources.limits.memory | RAM resource limits for Bamboo container     | 4Gi  |
| bamboo.resources.requests.cpu | CPU resource requests for Bamboo container     | 300m  |
| bamboo.resources.requests.memory | RAM resource requests for Bamboo container     | 1Gi  |
| bamboo.javaOpts.jvmMinimumMemory | xms for Bamboo     | 1024m  |
| bamboo.javaOpts.jvmMaximumMemory | xmx for Bamboo     | 4096m  |
| bamboo.jvmSupportRecommendedArgs | additional JVM arguments     | none  |

## Bamboo Agent

| Parameter        | Description      | Default Value |
| -----------------|:-------------:| |
| bamboo.agent.deploy  | deploy a remote agent      |  true |
| bamboo.agent.image.repository  | image repo      |  yivantsov/bamboo-base-agent |
| bamboo.agent.image.tag         | image tag       | see values.yaml  |
| bamboo.agent.image.pullPolicy  | image pull policy      | IfNotPresent  |
| bamboo.agent.resources.limits.cpu | CPU resource limits for Bamboo container     | 1000m  |
| bamboo.agent.resources.limits.memory | RAM resource limits for Bamboo container     | 5Gi  |
| bamboo.agent.resources.requests.cpu | CPU resource requests for Bamboo container     | 200m  |
| bamboo.agent.resources.requests.memory | RAM resource requests for Bamboo container     | 1Gi  |
| bamboo.agent.javaOpts.jvmMinimumMemory | xms for Bamboo     | 512m  |
| bamboo.agent.javaOpts.jvmMaximumMemory | xmx for Bamboo     | 4096m  |
| bamboo.agent.securityToken | server security token, auth is disabled by default     | none  |


## Bitbucket


| Parameter        | Description      | Default Value |
| -----------------|:-------------:| |
| bitbucket.deploy            | Deploy Atlassian Bitbucket | true         |
| bitbucket.datacenter        | Bitbucket in datacenter mode | false         |
| bitbucket.deployElasticSearch        | deploy Elasticsearch to a K8s cluster, in the same namespace | false         |
| bitbucket.elasticSearchUrl        | Elasticsearch url to use | http://elasticsearch:9200         |
| bitbucket.elasticsearchUsername        | Elasticsearch username | admin         |
| bitbucket.elasticsearchPassword        | Elasticsearhc password | password         |
| bitbucket.mode.install      | run initial installation, connect to DB, create tables. Set to true only during the 1st run | false         |
| bitbucket.image.repository  | image repo      |  atlassian/bitbucket-server |
| bitbucket.image.tag         | image tag       | see values.yaml  |
| bitbucket.image.pullPolicy  | image pull policy      | IfNotPresent  |
| bitbucket.license           | Bamboo license. Please mind indentation, see example in values.yaml      | none  |
| bitbucket.serverId          | your Bamboo server ID which comes with Bamboo license     | none  |
| bitbucket.serverKey         | your Bamboo server key      | 278  |
| bitbucket.adminUser         | your Bamboo admin user      | admin  |
| bitbucket.adminPassword     | your Bamboo admin password      | password  |
| bitbucket.adminFullName     | your Bamboo admin full name      | Bamboo Admin  |
| bitbucket.adminEmail        | your Bamboo admin email      | admin@atlassian.com  |
| bitbucket.db.type           | database type [ha/postgres]     | postgres  |
| bitbucket.db.deployPostgres | deploy Postgres or connect to existing database   | true  |
| bitbucket.db.host           | database host     | postgres  |
| bitbucket.db.port           | database port    | 5432  |
| bitbucket.db.username       | database username    | bitbucket  |
| bitbucket.db.password       | database password     | password  |
| bitbucket.db.database       | database name     | bitbucket  |
| bitbucket.ingress.enabled   | create ingress     | true  |
| bitbucket.ingress.annotations | ingress annotations     | ingressClass: nginx,  nginx.ingress.kubernetes.io/ssl-redirect: "true"  |
| bitbucket.ingress.host      | hostname for ingress spec     | git.kubedemo.ml  |
| bitbucket.storage.storageClassName      | StorageClass for Bamboo PVC     | none. default storage class will be used. Important! For Bitbucket datacenter PV should be RWX |
| bitbucket.storage.request      | request for storage     | 5Gi  |
| bitbucket.resources.limits.cpu | CPU resource limits for Bamboo container     | 3000m  |
| bitbucket.resources.limits.memory | RAM resource limits for Bamboo container     | 4Gi  |
| bitbucket.resources.requests.cpu | CPU resource requests for Bamboo container     | 300m  |
| bitbucket.resources.requests.memory | RAM resource requests for Bamboo container     | 1Gi  |
| bitbucket.javaOpts.jvmMinimumMemory | xms for Bamboo     | 1024m  |
| bitbucket.javaOpts.jvmMaximumMemory | xmx for Bamboo     | 4096m  |
| bitbucket.jvmSupportRecommendedArgs | additional JVM arguments     | none  |

Custom values can be passed in helm commands as `--set key=value`, for example `--set image.tag=custom`, or you can edit *values.yaml* before installing this Helm chart.

## How Installation Works

An init container is added to Bamboo deployment when `mode.install` is enabled. This init container mounts a ConfigMap with the initial `bamboo.cfg.xml` and copies it to a mounted directory.

If `mode.install` is enabled a K8s job is created to install Bamboo. This job runs a pod with a mounted ConfigMap that holds an installation script. The script makes a series of http requests against Bamboo internal endpoint to validate license, connect to a database of choice and create an admin user.

Bibucket uses built-in unattended installation mechanism.

## Adding More Bamboo Remote Agents

Bamboo remote agent deployment is enabled by default. It is a StatefulSet with 1 replica, which you can scale to as many replicas as you need (or your cluster can afford). This is how you add 3 remote agents:

```
kubectl scale statefulset --replicas-3 bamboo-agent -n atlassian
```

If you do not need some of your remote agents, run the above command and decrease the number of replicas.

By default agents to not use any authentication. If you want to use a security token, get one from your Bamboo server, and run Helm upgrade with `--set bamboo.agent.securityToken=yourToken`:

```
helm install atlassian ./ -n atlassian --set bamboo.mode.install=true --set bitbucket.mode.install=true --set bitbucket.datacenter=true --set link=true --set bamboo.agent.securityToken=yourToken
```

### Docker Tasks

A remote agent pod consists of 2 containers: remote agent itself, and a Docker in Docker container run in a privileged mode. Docker is installed in a remote agent image, and DOCKER_HOST env variable is exported, which points at Docker daemon running in Docker-in-Docker container (it is localhost - containers in a pod share storage and networking). There is no `/var/lib/docker` mount, so pulled images will be lost after agent pod restart (if this is really important, this mount can be easily added though).

### Custom Images for Remote Agents

It is possible that you do not want to use Docker tasks and the default agent image does not have all capabilities that your builds require. In this case, you may build your own image:

```
# tag differs
FROM yivantsov/bamboo-base-agent:7.0.4
# install software
RUN apt-get update && \
    apt-get install yourSoftware -y
# add files
ADD yourcustomfile /root/customfile
```

Once done, replace `yivantsov/bamboo-base-agent` with your own image and tag, and run `helm upgrade`. You may also copy `templates/statefulset-bamboo-agent.yaml`, change `bamboo-agent` names and run `helm upgrade`. Helm will deploy both the default agent (in case you need it) and your custom agent(s) which you can then scale separately as those will be 2 different StatefulSets.

## Bitbucket SSH URL

By default SSH URL isn't set, so Bitbucket will use `bitbucket.ingress.hostname` and port 7999 which will fail of course. Bamboo-bitbucket-link job will set it to `bitbucket.ingress.hostname` which means you will have to configure tcp port forwarding for your Nginx Ingress Controller https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/.

The ConfigMap will look as follows (provided that Bitbucket is deployed to `atlassian` namespace):

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: tcp-services
  namespace: ingress-nginx
data:
  22: "atlassian/bitbucket:7999"
```

Once done, you'll have to edit your ingress-nginx service and add an additional port:

```
- name: proxied-tcp-22
  port: 22
  targetPort: 22
  protocol: TCP
```

Once done, edit ingress-nginx deployment to make Nginx use a previously created ConfigMap:

```
args:
  - /nginx-ingress-controller
  - --tcp-services-configmap=ingress-nginx/tcp-services
```

It is possible not to use Nginx for tcp port forwarding. You can manually update SSH URL using NodePort and IP address of any of your K8s nodes:

```
kubectl get svc -n atlassian
NAME        TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)                                                       AGE
bamboo      NodePort   10.245.94.147    <none>        8085:30122/TCP,54663:30080/TCP,7896:30089/TCP                 5h34m
bitbucket   NodePort   10.245.70.20     <none>        7990:32464/TCP,7991:32679/TCP,7999:30081/TCP,7896:30596/TCP   5h34m
postgres    NodePort   10.245.138.121   <none>        5432:30351/TCP                                                5h34m
```

For, example, if IP of your K8s node is 10.4.30.23, your SSH Git URL will be:

```
ssh://10.4.30.23:30081
```
Bitbucket service is created as NodePort by default, and a NodePort is assigned to each declared port (it is not constant but stays the same as long as the service exists). Run, `kubectl get svc -n atlassian` to find out port mappings.

When on Minikube your IP would be the result of `minikupe ip` command.

## Scaling Bitbucket

Bitbucket is deployed as a StatefulSet, thus you can scale it:

```
kubectl scale statefulset --replicas-3 bitbucket -n atlassian
```

### PVC

**IMPORTANT!** All Bitbucket pods will use one shared Persistent Volume, thus it should support ***ReadWriteMany*** access mode, i.e. multiple pods should be able to mounts it simulaneously. **Not all storage classes support** this access mode. Minikube works fine though. Please, take a look at the default storage class your K8s provider has to offer and make sure it support RWX.

### Hazelcast

Even though there's an official Hazelcast plugin for Kubernetes to make discovery easy and fast, Bitbucket does not support it currently. Since Kubernetes network does not support **multicast** (at least most of them don't), `bitbucket.properties` needs to be updated with Bitbucket pod IPs when pods are created or deleted.

There's a special init container that will run a Python script `templates/cm-update-hazelcast-cluster-members.yaml` in the entrypoint, get IPs of all pods with `k8s-app=bitbucket` label and update `bitbucket.properties` file, before Bitbucket DC container start. This way all Bitbucket replicas have access to `bitbucket.properties` which is dynamically updated.

This isn't a proper solution but rather a workaround that **works well** though :) Waiting for official K8s Hazelcast support for Bitbucket!


## Troubleshooting

Putting everything in containers brings new challenges :)

### Logs

`kubectl logs` can be used to retreive logs from a selected pod or K8s job. Add `-f` to follow logs:

```
kubectl logs -f bamboo-3erjhds -n atlassian
```

Logging properties for Bamboo have been slightly modified so that logs aren't written to a file but streamed to stdout/err. Other log types are written to files.

Of course, log collectors and aggregators can be used. Fluentd and Filebeat worked OK with Elasticsearch. Of course, any K8s narive solution will work here.

### Kubernetes Events

Sometimes (or maybe often :) ) you will see pods stuck in ContainerCreating or Pending state. The same can happen to PVCs. This is where you'll need to get events.

Get all events in the namespace:

```
kubectl get events -n atlassian
```

You may get events for a particular pod:

```
kubectl describe pod bamboo-3edhshr -n atlassian
```

Run `kubectl get events --help` to get info on what filters you can apply to events.

### Debugging

Bamboo and Bitbucket are started in a debug mode by default (not debug log level!). Usually, a good way to attach to a process is use `kubectl port-forward` command, for example:

```
kubectl port-forward bamboo-3ertdh 7896:7896 -n atlassian
```

Attach your IDE to localhost:7896

### Monitoring

Deploy https://github.com/coreos/prometheus-operator to get Prometheus, AlterManager and Grafana.


## TODOs

A lot of those :)

* more docs :)
* make more things configurable (remote agent configuration, logging level for Bitbucket, maybe something from pod spec etc)
* make agent capabilities configurable, i.e. add a configMap with properties file.
* make it easy to deploy custom remote agents (create StatefulSets on the go. Ideally, all you need is to provide own image, and Helm will do the rest)
* do a better job with deploying Elasticsearch (perhaps use an official chart as a dependency)
* look into Postgres deployment to make it more prod ready (perhaps use an official Helm chart or Operator as a dependency)
* auto backup database and BAMBOO_HOME when upgrading to a new version
* restore from a backup on demand (maybe add a K8s CronJob that creates regular DB backups)
* use secrets for any passwords and sensitive information
* improve Python installers to use functions, better handle exceptions, retry failed requests etc. Current link-bamboo-bitbucket.py looks pretty bad :)
* make service type configurable, i.e. enable NodePort, Loadbancer and make Ingress optional
* TLS/SSL support for Bamboo and Bitbucket so that servers themselves run on https, not only Ingresses
* add prod features like PodDisruptionBudget (for example, always have at least 2 replicas of Bitbucket nodes in the cluster), Affinity/Anti-Affinity rules (for example always start Butbucket replicas on different nodes, or use only dedicated nodes for a particular service for better performance)
* better requests/limist/xmx/xms for all deployments
